#!/bin/bash

echo ""
echo "[RUNNING]: Reinstalling packages from deb"
echo ""

sudo dpkg --install --force-architecture --root=/data/data/com.team420.kekhunter/files/etc/root --admindir=/data/data/com.team420.kekhunter/files/etc/root/var/lib/dpkg debs/*.deb
