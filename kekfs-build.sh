#!/bin/bash

echo ""
echo "[RUNNING]: Building dpkg"
echo ""
rm /data/data/.built-packages/dpkg
./build-package.sh dpkg

echo ""
echo "[RUNNING]: Building bash"
echo ""
rm /data/data/.built-packages/bash
./build-package.sh bash

echo ""
echo "[RUNNING]: Building proot"
echo ""
rm /data/data/.built-packages/proot
./build-package.sh proot

echo ""
echo "[RUNNING]: Building wget"
echo ""
rm /data/data/.built-packages/wget
./build-package.sh wget

echo ""
echo "[RUNNING]: Building apt"
echo ""
rm /data/data/.built-packages/apt
./build-package.sh apt

echo ""
echo "[DONE]: Building kekfs"
echo ""
